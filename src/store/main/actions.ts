import {
    START_FETCH, FETCH_ERROR, MainActionTypes, GET_REVIEWS, StartFetchAction, GET_MAIN_PRICELIST, EQUIPMENT, GET_MALFUNCTIONS, SWITCH_FLAG
} from './types';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { AnyAction } from 'redux';
import { postRequest, getRequest } from '../../helpers';

type ThunkAct = ThunkAction<Promise<void>, {}, {}, AnyAction>;
type ThunkDisp = ThunkDispatch<{}, {}, AnyAction>;

export const sendReview = (
    addresser:string, city:string, text:string, cb:(res:any) => void
):ThunkAct => async (dispatch:ThunkDisp) => {

    fetchWrapper(
        dispatch,
        async () => {
            
            const res:any = await postRequest("/reviews/send", { addresser, city, text });
            
            cb(res.data);
            const success:MainActionTypes = {type: SWITCH_FLAG, payload: {field: "success_ld", val: res.data === "ok"}};
            dispatch(success);
        }
    )
}


export const leaveComment = (
    cb:(res:any) => void, text:string
):ThunkAct => async (dispatch:ThunkDisp) => {

    fetchWrapper(
        dispatch,
        async () => {
            
            const res:any = await postRequest("/comment/set", { text });
            
            cb(res.data);
            const success:MainActionTypes = {type: SWITCH_FLAG, payload: {field: "success_ld", val: res.data === "ok"}};
            dispatch(success);
        }
    )
}


export const leaveDetails = (
        cb:(res:any) => void, name:string, phone_number:string, 
        email?:string, malfunction_type?:string, equipment_type?:string, isQuestionare?:boolean
    ):ThunkAct => async (dispatch:ThunkDisp) => {

    fetchWrapper(
        dispatch,
        async () => {
            console.log(name, phone_number, email, 
                malfunction_type, equipment_type, isQuestionare);
            
            const res:any = await postRequest("/details/set", {
                name, phone_number, email, 
                malfunction_type, equipment_type, isQuestionare
            });
            
            cb(res.data);
            const success:MainActionTypes = {type: SWITCH_FLAG, payload: {field: "success_ld", val: res.data === "ok"}};
            dispatch(success);
        }
    )
}

export const getMalfunctions = (equipment:EQUIPMENT):ThunkAct => async (dispatch:ThunkDisp) => {
    fetchWrapper(
        dispatch,
        async () => {
            const res:any = await getRequest("/malfunctions/get", {equipment: equipment});
        
            const success:MainActionTypes = {type: GET_MALFUNCTIONS, payload: res.data};
            dispatch(success);
        }
    )
}

export const getMainPriceList = ():ThunkAct => async (dispatch:ThunkDisp) => {
    fetchWrapper(
        dispatch,
        async () => {
            const res:any = await getRequest("/malfunctions/get", {equipment: "all"});
        
            const success:MainActionTypes = {type: GET_MAIN_PRICELIST, payload: res.data};
            dispatch(success);
        }
    )
}

export const getReviews = ():ThunkAct => async (dispatch:ThunkDisp) => {
    fetchWrapper(
        dispatch,
        async () => {
            const res:any = await getRequest("/reviews/get", {});
        
            const success:MainActionTypes = {type: GET_REVIEWS, payload: res.data};
            dispatch(success);
        }
    );
}

const fetchWrapper = async (dispatch:ThunkDisp, mainAction: () => Promise<void>) => {
    const fetching:StartFetchAction = {type: START_FETCH};
    dispatch(fetching);   
    
    try {
        await mainAction();
    } catch(e) {
        const error:MainActionTypes = {type: FETCH_ERROR, payload: e };
        dispatch(error);
    }
}
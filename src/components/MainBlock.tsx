import React, { Component, ReactNode } from 'react';
import CTAModal from './modals/CTAModal';
import { connect } from 'react-redux';
import { leaveDetails } from '../store/main/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../store';
import ReactGA from 'react-ga';
import { RouteComponentProps, withRouter } from 'react-router-dom';

ReactGA.initialize('UA-164039795-1');

interface props extends RouteComponentProps {
    part_of_title: ReactNode;
    emergency_title?: string;
    emergency_min_price?: number;
    all_brand_title?: string;
    img_name: string;
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    isFetching: boolean;
}

class MainBlock extends Component<props, {}> {
    cta_modal: CTAModal | null;

    render() {
        const {
            part_of_title,
            emergency_title = 'бытовой техники',
            emergency_min_price = 150,
            all_brand_title = 'техники',
            img_name,
            leaveDetails,
            isFetching
        } = this.props;

        return (
            <div className="main-block">
                <div className="content">
                    <h1><span>БЫСТРЫЙ РЕМОНТ</span> {part_of_title} в Екатеринбурге <br/>на дому</h1>
                    <p className="descr">Наш сервисный центр предлагает квалифицированный ремонт бытовой техники с выездом специалиста уже в течение часа после утверждения заказа.</p>
                    <div className="for-swap">
                        <div className="services">
                            <div className="service">
                                <i className="departure"/>
                                <div>
                                    <p className="medium">Срочный ремонт {emergency_title} от {emergency_min_price} рублей</p>
                                </div>
                            </div>
                            <div className="service">
                                <i className="cracked-glass"/>
                                <div>
                                    <p className="medium">Мастер приедет уже с запчастями</p>
                                </div>
                            </div>
                            <div className="service">
                                <i className="wrench"/>
                                <div>
                                    <p className="medium">Ремонт {all_brand_title} всех брендов</p>
                                </div>
                            </div>
                            <div className="service">
                                <i className="official-guarantee"/>
                                <div>
                                    <p className="medium">Предоставляем официальные документы</p>
                                </div>
                            </div>
                        </div>
                        <button onClick={this.cta_modal?.modal?.showModal} className="btn">Вызвать мастера</button>
                    </div>
                </div>
                <div className={`img ${img_name}`}></div>
                <CTAModal 
                    isFetching={isFetching}
                    leaveDetails={leaveDetails}
                    ref={(node) => {this.cta_modal = node}} 
                    goToThankyou={() => {this.props.history.replace("/thankyou");}}
                />
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MainBlock));

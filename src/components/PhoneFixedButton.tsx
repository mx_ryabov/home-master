import React, { Component } from 'react'
import CTAModal from './modals/CTAModal';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { leaveDetails } from '../store/main/actions';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';

interface props extends RouteComponentProps {
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    isFetching: boolean;
}


class PhoneFixedButton extends Component<props, {}> {
    cta_modal: CTAModal | null;

    render() {
        return (
            <>
            <button 
                className="phone-fixed-button"
                onClick={this.cta_modal?.modal?.showModal}
            ></button>
            <CTAModal 
                isFetching={this.props.isFetching}
                leaveDetails={this.props.leaveDetails}
                ref={(node) => {this.cta_modal = node}} 
                goToThankyou={() => {this.props.history.replace("/thankyou");}}
            />
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PhoneFixedButton));


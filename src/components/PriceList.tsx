import React, { Component } from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import { Link } from 'react-router-dom';
import { IPriceList, IService } from '../store/main/types';

interface props {
    data: IPriceList[]
}

export default class PriceList extends Component<props, {}> {
    _renderPriceListItem() {
        return this.props.data.map((item:IPriceList, key:number) => {
            var cells = item.cells.map((cell:IService, keyCell:number) => {
                return (
                    <div className="cell" key={keyCell}>
                        <p>{cell.title}</p>
                        <span>от {cell.price}Р</span>
                    </div>
                )
            });
            const handleOnDragStart = (e:any) => e.preventDefault();
            return (
                <div className="price-list-item" key={key} onDragStart={handleOnDragStart}>
                    <h2>{item.title}</h2>
                    <div className="cells">
                        {cells}
                    </div>
                    <Link to={`/${item.eq_type.toLowerCase()}`} className="medium">Перейти в раздел</Link>
                </div>
            )
        })
    }
    render() {
        return (
            <div className="price-list" id="prices">
                <h1 className="light"><span className="bold">Цены</span> на наши услуги</h1>
                <div className="list">
                    <AliceCarousel 
                        mouseTrackingEnabled
                        responsive={{767: {items: 2}, 1335: {items: 3}}}
                    >
                        {this._renderPriceListItem.bind(this)()}
                    </AliceCarousel>
                </div>
            </div>
        )
    }
}


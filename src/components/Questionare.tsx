import React, { Component, ComponentState } from 'react';
import CTAModal from './modals/CTAModal';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { leaveDetails, leaveComment } from '../store/main/actions';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface props extends RouteComponentProps {
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string
    ) => Promise<void>;
    leaveComment: (
        cb:(res:any) => void, text:string
    ) => Promise<void>;
    isFetching: boolean;
}

interface state {
    currentQuestion: number;
    comment: string;
}

class Questionare extends Component<props, state> {
    state = {
        currentQuestion: 0,
        comment: ""
    }
    cta_modal:CTAModal | null;

    onHandleChange = (e:React.ChangeEvent<HTMLTextAreaElement>) => {
        let {name, value} = e.target;
        this.setState(
            {[name] : value} as ComponentState
        );
    }

    onAfterSubmit = (res:string) => {
        if (res === "ok") {
            this.setState({
                currentQuestion: 3
            });
        } else {
            this.setState({
                currentQuestion: 0
            });
        }
    }

    leaveComment = () => {
        this.props.leaveComment(this.onAfterSubmit, this.state.comment);
    }

    _renderQuestion = () => {
        switch(this.state.currentQuestion) {
            case -1:
                return (
                    <>
                        <div className="header">
                            <h2>Спасибо! Ждём вашего возвращения.</h2>
                        </div>
                    </>
                )

            case -2:
                return (
                    <>
                        <div className="header">
                            <h2>Пожалуйста, оставьте ваши контактные данные.</h2>
                        </div>
                        <div className="answers">
                            <button onClick={this.cta_modal?.modal?.showModal} className="btn">Оставить данные</button>
                        </div>
                    </>
                )

            case 2:
                return (
                    <>
                        <div className="header">
                            <h2>Что мы можем сделать, чтобы вы воспользовались нашими услугами?</h2>
                        </div>
                        <div className="answers">
                            <textarea 
                                placeholder="Пишите здесь" 
                                name="comment"
                                value={this.state.comment}
                                onChange={this.onHandleChange}
                            ></textarea>
                            <button onClick={this.leaveComment} className="btn">Отправить</button>
                        </div>
                    </>
                )

            case 3:
                return (
                    <>
                        <div className="header">
                            <h2>Спасибо за ваше мнение!</h2>
                        </div>
                        <div className="answers">
                            <button onClick={this.cta_modal?.modal?.showModal} className="btn">Оставить контакт</button>
                        </div>
                    </>
                )

            default:
                let q:any = questions[this.state.currentQuestion];
                return (
                    <>
                        <div className="header">
                            <h2>{q.text}</h2>
                            <div className="q_num">{this.state.currentQuestion + 1}</div>
                        </div>
                        <div className="answers">
                            {
                                q.answers.map((ans:any, ind:number) => {
                                    return (
                                        <button 
                                            className="btn" 
                                            key={ind}
                                            onClick={() => this.setState({currentQuestion: ans.next})}
                                        >{ans.title}</button>
                                    )
                                })
                            }
                        </div>
                    </>
                )
        }
        
    }

    render() {
        return (
            <div className="questionare">
                {this._renderQuestion()}
                <CTAModal 
                    isFetching={this.props.isFetching}
                    leaveDetails={this.props.leaveDetails}
                    ref={(node) => {this.cta_modal = node}}
                    goToThankyou={() => {this.props.history.replace("/thankyou");}}
                />
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number, undefined, undefined, undefined, true
        ));
    },
    leaveComment: async (
        cb:(res:any) => void, text:string
    ) => {
		dispatch(leaveComment(
            cb, text
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Questionare));

const questions = [
    { 
        text: "Есть ли у вас прибор, который нужно починить?",
        answers: [
            { title: "ДА", next: 1, type: "btn" },
            { title: "НЕТ", next: -1, type: "btn" }
        ]
    },
    { 
        text: "Вы заинтересованы в нашей бесплатной консультации?",
        answers: [
            { title: "ДА", next: -2, type: "btn" },
            { title: "НЕТ", next: 2, type: "btn" }
        ]
    }
]
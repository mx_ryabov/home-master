import React, { Component, ComponentState } from 'react';
import Select from './Select';
import { EQUIPMENT, IMalfunctionType } from '../store/main/types';
import InfoModal from './modals/InfoModal';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { leaveDetails } from '../store/main/actions';
import { connect } from 'react-redux';
import InputMask from 'react-input-mask';
import ReactGA from 'react-ga';
import axios from 'axios';
import ym from 'react-yandex-metrika';
import { RouteComponentProps, withRouter } from 'react-router-dom';

ReactGA.initialize('UA-164039795-1');

interface props extends RouteComponentProps {
    equipment_type: EQUIPMENT;
    eq_type_rus: string;
    malfunction_types: IMalfunctionType[];
    leaveDetails: (
        cb:(res:any) => void, name:string, phone_number:string, 
        email:string, malfunction_type:string, equipment_type:EQUIPMENT
    ) => Promise<void>;
    isFetching: boolean;
}

interface state {
    malfunction_type: IMalfunctionType;
    name:string;
    phone_number:string;
    email: string;
    errors: {
        name: string;
        phone_number: string;
        email: string;
        malfunction_type: string;
    };
    isFormValid: boolean;
}

class CostCalculation extends Component<props, state> {
    state = {
        name: "",
        phone_number: "",
        email: "",
        malfunction_type: {title: "Выберите неисправность", pk: "0"},
        errors: {
            name: "",
            phone_number: "",
            email: "",
            malfunction_type: ""
        },
        isFormValid: false
    }
    info_modal:InfoModal | null;

    onSelectChange(selected:IMalfunctionType) {
        this.setState(
            {malfunction_type: selected},
            () => this.validatingForm("malfunction_type")
        );       
    }
    /* for FORM */

    onHandleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
        let {name, value} = e.target;
        this.setState(
            {[name] : value} as ComponentState, 
            () => this.validatingForm(name)
        );
    }

    setError = (field: "name" | "phone_number" | "email" | "malfunction_type", mess: string) => {
        this.setState({
            errors: {
                ...this.state.errors,
                [field]: mess
            }
        }, () => {
            let lenErrors:number = 0;
            for (let field in this.state.errors) {
                lenErrors += Reflect.get(this.state.errors, field).length;            
            }
            this.setState({isFormValid: lenErrors === 0})
        })
    }

    validatingForm = async (field: string) => {
        switch(field) {
            case "name":
                await this.setError("name", 
                    this.state.name.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;
            
            case "phone_number":
                await this.setError("phone_number", 
                    this.state.phone_number.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;

            case "email":
                await this.setError("email", 
                    this.state.email.length === 0 ? "Поле обязательное для ввода" : ""
                );
                break;

            case "malfunction_type":
                await this.setError("malfunction_type", 
                    this.state.malfunction_type.pk === "0" ? "Выберите подходящий вариант" : ""
                );
                break;

            default:
                break;
        }
    }

    onAfterSubmit = (res:string) => {
        if (res === "ok") {
            /*this.info_modal?.showModal(
                "Заявка успешно отправлена!",
                "Мы перезвоним Вам в течение 15 минут. Наш специалист ответит на все интересующие вопросы."
            )*/
            ReactGA.event({
                category: "Отправка контактов",
                action: "От правка с указанием почты и неисправности"
            });
            this.props.history.replace("/thankyou");
        } else {
            this.info_modal?.showModal(
                "Произошла ошибка...",
                "Попробуйте заполнить форму ещё раз."
            )
        }
        this.setState({
            name: "",
            phone_number: "",
            email: "",
            malfunction_type: {title: "Выберите неисправность", pk: "0"},

            errors: {
                name: "",
                phone_number: "",
                email: "",
                malfunction_type: ""
            },
            isFormValid: false
        })
    }

    onSubmit = async () => {
        for (let field in this.state.errors) {
            await this.validatingForm(field);          
        }
        if (this.state.isFormValid) {
            await this.props.leaveDetails(
                this.onAfterSubmit,
                this.state.name,
                this.state.phone_number,
                this.state.email,
                this.state.malfunction_type.pk,
                this.props.equipment_type
            );
            ym('reachGoal', 'all_form');
        }
    }
    /* ENF for FORM */

    render() {
        return (
            <div className="cost-calculation" id="prices">
                <h1 className="light"><span className="bold">Расчёт стоимости</span> услуги</h1>
                <p>Для оформления заявки на ремонт стиральной машины, заполните форму. Мы перезвоним вам в течении 15 минут!</p>
                <div className="form">
                    <div className="fields">
                        <div className={`wrapper ${this.state.errors.name.length > 0 ? "error" : ""}`}>
                            <input 
                                type="text" 
                                placeholder="Ваше имя" 
                                name="name"
                                value={this.state.name}
                                onChange={this.onHandleChange}
                            />
                            <span>{this.state.errors.name}</span>
                        </div>
                        <div className={`wrapper ${this.state.errors.phone_number.length > 0 ? "error" : ""}`}>
                            <InputMask
                                name="phone_number"
                                type="tel"
                                placeholder="+7 (___)__-__-___" 
                                mask="+7 (999) 999-99-99"
                                value={this.state.phone_number}
                                onChange={this.onHandleChange}/>
                            <span>{this.state.errors.phone_number}</span>
                        </div>
                    </div>
                    <div className="fields">
                        <div className={`wrapper ${this.state.errors.email.length > 0 ? "error" : ""}`}>
                            <input 
                                type="text"
                                placeholder="Email"
                                name="email"
                                value={this.state.email}
                                onChange={this.onHandleChange}
                            />
                            <span>{this.state.errors.email}</span>
                        </div>
                        <div className={`wrapper ${this.state.errors.malfunction_type.length > 0 ? "error" : ""}`}>
                            <Select<IMalfunctionType>
                                onChange={this.onSelectChange.bind(this)}
                                options={this.props.malfunction_types}
                                value={this.state.malfunction_type}
                                placeholder="Выберите неисправность"
                            />
                            <span>{this.state.errors.malfunction_type}</span>
                        </div>
                    </div>
                    <div className="fields">
                        <p className="medium">
                            Нажимая на кнопку, вы соглашаетесь с<br/>
                            <a className="medium" href="/privacy-policy">политикой обработки персональных данных</a>
                        </p>
                        <button 
                            onClick={this.onSubmit} 
                            className="btn"
                            disabled={this.props.isFetching}
                        >Отправить</button>
                    </div>
                </div>
                <InfoModal 
                    ref={(node) => this.info_modal = node} 
                />
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    malfunction_types: state.main.malfunction_types,
    isFetching: state.main.fetching
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	leaveDetails: async (
        cb:(res:any) => void, name:string, phone_number:string, 
        email:string, malfunction_type:string, equipment_type:EQUIPMENT
    ) => {
		dispatch(leaveDetails(
            cb, name, phone_number, 
            email, malfunction_type, equipment_type
        ));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CostCalculation));
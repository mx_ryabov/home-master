import React, { Component } from 'react';
import InfoModal from './modals/InfoModal';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface props extends RouteComponentProps {}

class InfoAlert extends Component<props, {}> {
    info_modal:InfoModal | null;

    componentDidMount() {        
        if (this.props.history.location.pathname !== "/privacy-policy") {
            let infoModalTimer:string | null = localStorage.getItem('info_alert');
            if (infoModalTimer) {
                let oldDate:Date = new Date(infoModalTimer);
                let now:Date = new Date();
                
                if ((now.valueOf() - oldDate.valueOf())/1000/60 > 2) {
                    localStorage.setItem('info_alert', new Date().toString());
                    this.info_modal?.showModal(
                        "При заказе с сайта ремонт со скидкой 20%", 
                        `Выезд в течение 35 минут, мастер опоздал — ремонт за счет компании.`
                    );
                }
            } else {
                localStorage.setItem('info_alert', new Date().toString());
                this.info_modal?.showModal(
                    "При заказе с сайта ремонт со скидкой 20%", 
                    `Выезд в течение 35 минут, мастер опоздал — ремонт за счет компании.`
                );
            }
        }
        
    }

    render() {
        return (
            <InfoModal ref={node => this.info_modal = node} />
        )
    }
}

export default withRouter(InfoAlert);
import React, { Component } from 'react';
import Header from '../components/Header';
import MainBlock from '../components/MainBlock';
import AboutUs from '../components/AboutUs';
import Brands from '../components/Brands';
import Reviews from '../components/Reviews';
import Questionare from '../components/Questionare';
import ContactUsForm from '../components/ContactUsForm';
import Footer from '../components/Footer';
import WhatProblem from '../components/WhatProblem';
import { IMalfunction, EQUIPMENT } from '../store/main/types';
import CostCalculation from '../components/CostCalculation';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { connect } from 'react-redux';
import { getMalfunctions } from '../store/main/actions';
import PhoneFixedButton from '../components/PhoneFixedButton';

interface props {
    getMalfunctions: (equipment:EQUIPMENT) => Promise<void>;
    malfunctions: IMalfunction[];
}


class RefrigeratorPage extends Component<props, {}> {
    componentDidMount() {
        this.props.getMalfunctions("Refrigerator");
    }

    render() {
        return (
            <>
                <Header />
                <MainBlock
                    part_of_title={(<>холодильников <br/></>)}
                    emergency_title={'холодильников'}
                    emergency_min_price={450}
                    all_brand_title={'холодильников'}
                    img_name="refrigerator"
                />
                <WhatProblem
                    problem_types={this.props.malfunctions}
                />
                <CostCalculation equipment_type="Refrigerator" eq_type_rus="Холодильник" />
                <AboutUs />
                <Brands />
                <Reviews />
                <Questionare />
                <ContactUsForm />
                <PhoneFixedButton />
                <Footer />
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
	malfunctions: state.main.malfunctions
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getMalfunctions: async (equipment:EQUIPMENT) => {
		dispatch(getMalfunctions(equipment));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(RefrigeratorPage);

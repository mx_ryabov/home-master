import React, { Component } from 'react';
import Header from '../components/Header';
import MainBlock from '../components/MainBlock';
import PriceList from '../components/PriceList';
import AboutUs from '../components/AboutUs';
import Brands from '../components/Brands';
import Reviews from '../components/Reviews';
import Questionare from '../components/Questionare';
import ContactUsForm from '../components/ContactUsForm';
import Footer from '../components/Footer';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { IPriceList } from '../store/main/types';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { getMainPriceList } from '../store/main/actions';
import { connect } from 'react-redux';
import InfoModal from '../components/modals/InfoModal';
import PhoneFixedButton from '../components/PhoneFixedButton';

interface props extends RouteComponentProps {
    getMainPriceList: () => Promise<void>;
    main_price_list: IPriceList[];
}

class MainPage extends Component<props, {}> {
    thankYouModal:InfoModal | null;

    componentDidMount() {
        this.props.getMainPriceList();

    }

    render() {
        if (this.props.location.pathname.split('/').pop() === "thankyou") {
            this.thankYouModal?.showModal(
                "Заявка успешно отправлена!",
                "Мы перезвоним Вам в течение 15 минут. Наш специалист ответит на все интересующие вопросы.",
                () => {this.props.history.replace("/")}
            )
        }
        return (
            <>
                <Header />
                <MainBlock
                    part_of_title={(<>бытовой <br/>техники</>)}
                    img_name="main"
                />
                <div className="our-services" id="our_services">
                    <h1 className="light"><span className="bold">С чем у Вас</span> проблема?</h1>
                    <div className="cards">
                        <Link to="/washer" className="card">
                            <i className="washer"></i>
                            <p className="medium">Стиральная машина</p>
                        </Link>
                        <Link to="/dishwasher" className="card">
                            <i className="dishwasher"></i>
                            <p className="medium">Посудомоечная машина</p>
                        </Link>
                        <Link to="/refrigerator" className="card">
                            <i className="refrigerator"></i>
                            <p className="medium">Холодильник</p>
                        </Link>
                        <Link to="/oven" className="card">
                            <i className="oven"></i>
                            <p className="medium">Духовой шкаф</p>
                        </Link>
                        <Link to="/hob" className="card">
                            <i className="hob"></i>
                            <p className="medium">Варочная поверхность</p>
                        </Link>
                        <Link to="/other" className="card">
                            <i className="other"></i>
                            <p className="medium">Другая техника</p>
                        </Link>
                    </div>
                </div>
                <PriceList data={this.props.main_price_list} />
                <AboutUs />
                <Brands />
                <Reviews />
                <Questionare />
                <ContactUsForm />
                <Footer />
                <InfoModal ref={node => this.thankYouModal = node} />
                <PhoneFixedButton />
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
	main_price_list: state.main.main_price_list
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getMainPriceList: async () => {
		dispatch(getMainPriceList());
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MainPage));


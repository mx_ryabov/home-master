import React, { Component } from 'react';
import Header from '../components/Header';
import MainBlock from '../components/MainBlock';
import AboutUs from '../components/AboutUs';
import Brands from '../components/Brands';
import Reviews from '../components/Reviews';
import Questionare from '../components/Questionare';
import ContactUsForm from '../components/ContactUsForm';
import Footer from '../components/Footer';
import WhatProblem from '../components/WhatProblem';
import CostCalculation from '../components/CostCalculation';
import { IMalfunction, EQUIPMENT } from '../store/main/types';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { getMalfunctions } from '../store/main/actions';
import { connect } from 'react-redux';
import PhoneFixedButton from '../components/PhoneFixedButton';

interface props {
    getMalfunctions: (equipment:EQUIPMENT) => Promise<void>;
    malfunctions: IMalfunction[];
}

class DishwasherPage extends Component<props, {}> {
    componentDidMount() {
        this.props.getMalfunctions("Dishwasher");
    }
 
    render() {
        return (
            <>
                <Header />
                <MainBlock
                    part_of_title={(<>посудомоечных <br/>машин</>)}
                    emergency_title={'посудомоечных машин'}
                    all_brand_title={'посудомоечных машин'}
                    img_name="dishwasher"
                />
                <WhatProblem
                    problem_types={this.props.malfunctions}
                />
                <CostCalculation
                    equipment_type="Dishwasher" 
                    eq_type_rus="Посудомоечная машина"
                />
                <AboutUs />
                <Brands />
                <Reviews />
                <Questionare />
                <ContactUsForm />
                <PhoneFixedButton />
                <Footer />
            </>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
	malfunctions: state.main.malfunctions
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getMalfunctions: async (equipment:EQUIPMENT) => {
		dispatch(getMalfunctions(equipment));
	},
});

export default connect(mapStateToProps, mapDispatchToProps)(DishwasherPage);

